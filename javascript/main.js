"use strict";
let wasm_module;

module.exports.loop = function() {
	try {
		if (wasm_module && wasm_module.__wasm) {
			wasm_module.game_loop();
		} else {
			if (Game.cpu.tickLimit < 300) {
				console.log("we are running out of time, pausing compile " + JSON.stringify(Game.cpu.tickLimit) + "/500");
				return;
			}
			console.log("RESET");
			wasm_module = require("qscreeps");
			wasm_module.initialize_instance();
			wasm_module.setup();
			wasm_module.game_loop();
		}
	} catch {
		wasm_module.__wasm = null;
	}
}
