#![feature(decl_macro)]
#![allow(clippy::option_map_unit_fn,clippy::comparison_chain)]

use std::collections::{BinaryHeap,HashMap};
use std::ops::*;
use std::fmt;
use std::mem::transmute;

use wasm_bindgen::prelude::*;
use screeps::{LineStyle,HasPosition,PathFinder,SearchOptions,MultiRoomCostResult};

mod utils; use utils::*;
mod path; use path::*;

#[wasm_bindgen]
pub fn setup() {
    let start = cpu_used();
    set_panic_hook();
    for name_int in screeps::rooms().keys() {
        let name: RoomName = name_int.into();
        for name in name.adj() {
            for name in name.adj() {
                for name in name.adj() {
                    if !terrain_is_init(name) {
                        info!("initializing {}",name);
                        init_terrain(name);
                    }
                }
            }
        }
    }
    info!("done setup in {}",cpu_used() - start);
}

#[wasm_bindgen]
pub fn game_loop() {
    if cpu_bucket() > 500 {
        let spawn = screeps::spawns().values().next().unwrap();
        let spawn_pos: Pos = spawn.pos().pos().into();
        
        let mut paths = [
            ((0, 0), (0, 10)),
            ((0, 0), (10, 10)),
            ((0, 0), (10, -10)),
            ((0, 0), (-20, -20)),
            ((0, 0), (50, 50)),
            //((0, 0), (-100, -100)),
            ((50, 50), (50, -50)),
        ].map(|(from, to)| (spawn_pos + from, spawn_pos + to));
        
        for (from, to) in paths.iter_mut() { //no paths into walls
            while tile(*from) == Rock {
                *from = *from + from.dir_to(spawn_pos);
            }
            while tile(*to) == Rock {
                *to = *to + to.dir_to(spawn_pos);
            }
        }
        
        info!("\nbenchmarking {} paths",paths.len());
        
        //let visual = screeps::RoomVisual::new(None);
        
        info!("\nrust:");
        let start = cpu_used();
        let mut settings = PathSettings::new(5, 10, false, 0.0);
        settings.distance_bias = settings.cost(Plain); //to make the heuristic bad for roads, the same way the default pathfinder does, still better than the default pathfinder tho bc roads still have a lower cost
        for &(from, to) in &paths {
            let start = cpu_used();
            if let Some(path) = settings.path(from, to) {
                info!("pathed from {} to {} in {} steps and {} cpu",from,to,path.steps.len(),cpu_used() - start);
                /*if from.room_name() == to.room_name() {
                    path.traverse(from, |p, dir| {
                        let p2 = p + dir;
                        visual.line((p.x() as f32, p.y() as f32), (p2.x() as f32, p2.y() as f32), Some(LineStyle::default().color("blue")));
                    })
                }*/
            } else {
                info!("failed to path from {} to {}",from,to);
            }
        }
        info!("rust pathfinder took {}",cpu_used() - start);
        
        info!("\nc++:");
        let start = cpu_used();
        for &(from, to) in &paths {
            let start = cpu_used();
            let options = SearchOptions::new(|_| MultiRoomCostResult::Default)
                .max_ops(1000000)
                .heuristic_weight(1.0)
                //.plain_cost(2)
                //.swamp_cost(10)
                .max_rooms(100);
            let result = screeps::search(from.pos(), to.pos(), 0, Some(options));
            info!("pathed from {} to {} in {} steps and {} cpu",from,to,result.path().len(),cpu_used() - start);
        }
        info!("C++ pathfinder took {}",cpu_used() - start);
    }
}

fn set_panic_hook() {
    std::panic::set_hook(Box::new(|info| {
        let msg = info.to_string();
        error!("{}",msg);
        /*info!("stack:");
        let e = Error::new();
        info!("{}",e.stack());*/
    }));
}
