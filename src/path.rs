use crate::*;

const MAX_COST: u16 = 20000;
const ROAD_COST: f32 = 0.05;
const INTENT_COST: f32 = 1.0;

#[repr(u8)]
#[derive(Copy,Clone,Eq,PartialEq,Debug)]
pub enum Tile {
    Plain = 0,
    Rock = 1,
    Swamp = 2,
    Blocked = 3,
    Pruned = 4,
    DangerPlain = 5,
    DangerSwamp = 6,
    PlainRoad = 7,
    SwampRoad = 8,
    PrunedRoad = 9,
    DangerRoad = 10,
    Exit = 11,
}
pub use Tile::*;

impl Default for Tile {
    fn default() -> Self {
        Plain
    }
}

impl Tile {
    pub fn from_num(x: u8) -> Self {
        match x {
            0 => Plain,
            1 => Rock,
            2 => Swamp,
            3 => Blocked,
            4 => Pruned,
            5 => DangerPlain,
            6 => DangerSwamp,
            7 => PlainRoad,
            8 => SwampRoad,
            9 => PrunedRoad,
            10 => DangerRoad,
            11 => Exit,
            _ => unreachable!(),
        }
    }
    
    pub fn is_passable(self) -> bool {
        self != Rock && self != Blocked
    }
    
    pub fn set_road(self) -> Self {
        match self {
            Plain => PlainRoad,
            Swamp | Rock => SwampRoad,
            Pruned => PrunedRoad,
            DangerPlain | DangerSwamp => DangerRoad,
            x => x,
        }
    }
    pub fn has_road(self) -> bool {
        [PlainRoad,SwampRoad,PrunedRoad,DangerRoad].contains(&self)
    }
    pub fn road_maint_mul(self) -> f32 {
        match self {
            PlainRoad | Plain => 1.0,
            Rock => 125.0,
            _ => 5.0,
        }
    }
    
    pub fn fatigue_mul(self) -> u16 {
        if self.is_passable() {
            match self {
                Plain | DangerPlain => 2,
                Swamp | DangerSwamp | Pruned => 10,
                Exit => 0,
                _ => 1,
            }
        } else {
            255
        }
    }
    
    pub fn speed_with(self, move_parts: u16, mass: u16) -> u16 {
        let fatigue = mass * self.fatigue_mul();
        fatigue.saturating_sub(1) / (move_parts * 2) + 1
    }
}

#[derive(Default,Clone,Copy)]
pub struct PathSettings {
    costs: [u16; 12],
    pub distance_bias: u16,
    //TODO
    //pub cost_mod: ...,
    //pub bias_mod: ...,
}

impl PathSettings { //TODO: profile Vec2<u16> vs Pos vs Position
    pub fn new(move_parts: u16, mass: u16, road_maint: bool, intent_cost: f32) -> Self {
        let road_maint = road_maint as u8 as f32 * ROAD_COST;
        let intent_cost = intent_cost * INTENT_COST;
        let mut r = Self::default();
        for i in 0..r.costs.len() {
            let tile = Tile::from_num(i as u8);
            let mut cost = tile.speed_with(move_parts, mass) as f32;
            cost += intent_cost;
            if !tile.is_passable() {
                r.costs[i] = MAX_COST;
                continue;
            } else if tile.has_road() {
                cost += road_maint * tile.road_maint_mul();
            }
            r.costs[i] = (cost * 10.0) as u16;
        }
        r.calc_distance_bias();
        r
    }
    
    pub fn assume_roads(mut self) -> Self { //doesn't assume rocks are roads, obviously xd
        self.costs[Plain as usize] = self.costs[PlainRoad as usize];
        self.costs[Swamp as usize] = self.costs[SwampRoad as usize];
        self.costs[Pruned as usize] = self.costs[PrunedRoad as usize];
        self.costs[DangerPlain as usize] = self.costs[DangerRoad as usize];
        self.costs[DangerSwamp as usize] = self.costs[DangerRoad as usize];
        self.calc_distance_bias();
        self
    }
    
    pub fn cost(&self, tile: Tile) -> u16 {
        self.costs[tile as usize]
    }
    
    fn calc_distance_bias(&mut self) {
        self.distance_bias = u16::max_value();
        for x in self.costs {
            self.distance_bias = self.distance_bias.min(x);
        }
    }
    
    pub fn path(self, from: Pos, to: Pos) -> Option<Path> {
        start_new_path(from);
        let mut unsearched = BinaryHeap::with_capacity(512);
        let bias = self.distance_bias * from.range_to(to) as u16;
        set_searched(from, Top, bias);
        unsearched.push(Node { from: TopLeft, pos: from, cost: bias, bias });
        unsearched.push(Node { from: BottomRight, pos: from, cost: bias, bias });
        while let Some(parent) = unsearched.pop() {
            if let Some(path) = self.search_node(from, to, &mut unsearched, parent) {
                return Some(path);
            }
        }
        None
    }
    
    #[inline(always)]
    fn search_node(self, from: Pos, to: Pos, unsearched: &mut BinaryHeap<Node>, parent: Node) -> Option<Path> {
        for &dir in &SEARCH_DIRS[parent.from as usize] {
            let p = parent.pos + dir;
            let (_, orig_cost) = get_searched(p).unwrap_or((Top, MAX_COST));
            if orig_cost <= parent.cost { //early out
                continue;
            }
            if p == to {
                return Some(backtrace_path(from, p, parent));
            }
            let bias = self.distance_bias * p.range_to(to) as u16;
            let cost = parent.cost + self.cost(tile(p)) + bias - parent.bias;
            if cost < orig_cost {
                let node = Node {
                    from: dir,
                    pos: p,
                    bias,
                    cost,
                };
                set_searched(p, dir, node.cost);
                unsearched.push(node);
                //why does this break the algorithm??
                /*if unsearched.peek().is_none() || node >= *unsearched.peek().unwrap() {
                    self.search_node2(from, to, unsearched, node);
                } else {
                    unsearched.push(node);
                }*/
            }
        }
        None
    }
    
    /*#[inline(always)]
    fn search_node2(self, from: Pos, to: Pos, unsearched: &mut BinaryHeap<Node>, parent: Node) -> Option<Path> {
        for &dir in &SEARCH_DIRS[parent.from as usize] {
            let p = parent.pos + dir;
            let (_, orig_cost) = get_searched(p).unwrap_or((Top, MAX_COST));
            if orig_cost <= parent.cost { //early out
                continue;
            }
            if p == to {
                return Some(backtrace_path(from, p, parent));
            }
            let cost = parent.cost + self.cost(tile(p)) + self.distance_bias * (p.range_to(to) - parent.pos.range_to(to)) as u16; //TODO: seperate field for bias?
            if cost < orig_cost {
                let node = Node {
                    from: dir,
                    pos: p,
                    cost,
                };
                set_searched(p, dir, node.cost);
                unsearched.push(node);
                /*if node.cost <= unsearched.peek().map(|x| x.cost).unwrap_or(0) {
                    self.search_node2(from, to, unsearched, node);
                } else {
                    unsearched.push(node);
                }*/
            }
        }
        None
    }*/
}

const SEARCH_DIRS: [[Dir; 5]; 8] = [
    [Top,TopRight,TopLeft,TopLeft,TopLeft],
    [TopRight,BottomRight,TopLeft,Top,Right],
    [Right,TopRight,BottomRight,BottomRight,BottomRight],
    [BottomRight,TopRight,BottomRight,Right,Bottom],
    [Bottom,BottomRight,BottomLeft,BottomLeft,BottomLeft],
    [BottomLeft,BottomRight,TopLeft,Bottom,Left],
    [Left,BottomLeft,TopLeft,TopLeft,TopLeft],
    [TopLeft,TopRight,BottomLeft,Top,Left],
];

#[derive(Default)]
pub struct Path {
    pub dest: Pos,
    pub steps: HashMap<Pos, Dir>,
    pub cost: u16,
}

impl Path {
    pub fn next(&self, pos: Pos) -> Option<Dir> {
        self.steps.get(&pos).cloned()
    }
    
    pub fn traverse<F: FnMut(Pos, Dir)>(&self, from: Pos, mut f: F) {
        let mut p = from;
        while let Some(&dir) = self.steps.get(&p) {
            f(p, dir);
            p = p + dir;
            if p == from { break; }
        }
    }
}

#[derive(Copy,Clone)]
pub struct Node {
    pos: Pos,
    bias: u16,
    cost: u16,
    from: Dir,
}
use std::cmp::*;
impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.cost == other.cost
    }
}
impl Eq for Node {}
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        let a = ((self.cost as u32) << 16) | self.bias as u32;
        let b = ((other.cost as u32) << 16) | other.bias as u32;
        b.cmp(&a)
    }
}

fn backtrace_path(from: Pos, dest: Pos, parent: Node) -> Path {
    let start = cpu_used();
    let mut steps = HashMap::new();
    let mut pos = parent.pos;
    steps.insert(pos, pos.dir_to(dest));
    while pos != from {
        let (dir, _cost) = get_searched(pos).unwrap();
        pos = pos - dir;
        steps.insert(pos, dir);
        //TODO: fix this for when exit jumping is enabled
        //assert_eq!(node.pos.dir_to(pos), dir);
        //assert_eq!(pos - dir, node.pos);
    }
    Path {
        dest,
        steps,
        cost: parent.cost,
    }
}

const SUB_ROOM_SIZE: usize = 32;
static mut PATH_GEN: u16 = 0;
//static mut PATH_SRC: Pos = Pos(0, 0);
//TODO: use a Dir instead of Node to reduce memory use?
static mut SEARCHED: [[[[(u16, u16); SUB_ROOM_SIZE]; SUB_ROOM_SIZE]; WORLD_SIZE / SUB_ROOM_SIZE]; WORLD_SIZE / SUB_ROOM_SIZE] = [[[[(0, 0); SUB_ROOM_SIZE]; SUB_ROOM_SIZE]; WORLD_SIZE / SUB_ROOM_SIZE]; WORLD_SIZE / SUB_ROOM_SIZE];

fn start_new_path(_from: Pos) {
    unsafe {
        //PATH_SRC = from - (SEARCHED.len() as i16 / 2, SEARCHED.len() as i16 / 2);
        PATH_GEN = PATH_GEN.wrapping_add(8);
        if PATH_GEN == 0 {
            SEARCHED = [[[[(0, 0); SUB_ROOM_SIZE]; SUB_ROOM_SIZE]; WORLD_SIZE / SUB_ROOM_SIZE]; WORLD_SIZE / SUB_ROOM_SIZE];
            PATH_GEN = 8;
        }
    }
}

fn set_searched(p: Pos, dir: Dir, cost: u16) {
    unsafe {
        //let (x, y) = p.relative_to(PATH_SRC);
        let (x, y) = (p.0, p.1);
        *SEARCHED.get_unchecked_mut(x as usize / SUB_ROOM_SIZE).get_unchecked_mut(y as usize / SUB_ROOM_SIZE)
            .get_unchecked_mut(x as usize % SUB_ROOM_SIZE).get_unchecked_mut(y as usize % SUB_ROOM_SIZE)
            = (PATH_GEN | dir as u16, cost);
    }
}

fn get_searched(p: Pos) -> Option<(Dir, u16)> {
    unsafe {
        //let (x, y) = p.relative_to(PATH_SRC);
        let (x, y) = (p.0, p.1);
        let (r, cost) = *SEARCHED.get_unchecked(x as usize / SUB_ROOM_SIZE).get_unchecked(y as usize / SUB_ROOM_SIZE)
            .get_unchecked(x as usize % SUB_ROOM_SIZE).get_unchecked(y as usize % SUB_ROOM_SIZE);
        if r >= PATH_GEN {
            Some((Dir::from(r), cost))
        } else {
            None
        }
    }
}
