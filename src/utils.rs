use crate::*;
use screeps::{Position,RoomPosition as JsPos,RoomName as RoomNameInt};

pub const WORLD_ROOMS: usize = 61;
pub const WORLD_SIZE: usize = WORLD_ROOMS * 2 * 50;

pub fn cpu_bucket() -> i32 { screeps::cpu::bucket() }
pub fn cpu_used() -> f64 { screeps::cpu::get_used() }

pub static mut TERRAIN: [[Tile; WORLD_SIZE]; WORLD_SIZE] = [[Plain; WORLD_SIZE]; WORLD_SIZE];
pub fn tile(p: Pos) -> Tile { unsafe {
    *TERRAIN.get_unchecked(p.0 as usize).get_unchecked(p.1 as usize)
}}
pub fn set_tile(p: Pos, t: Tile) { unsafe {
    TERRAIN[p.0 as usize][p.1 as usize] = t;
}}
pub fn terrain_is_init(name: RoomName) -> bool {
    tile(Pos::new(0, 0, name)) as u8 != 0
}

pub fn init_terrain(name: RoomName) {
    if terrain_is_init(name) { return; }
    let mut terrain = [0; 2500];
    let raw = screeps::map::get_room_terrain(name.int()).get_raw_buffer();
    raw.copy_to(&mut terrain);
    for x in 0..50 {
        for y in 0..50 {
            let mut n = terrain[x+y*50];
            if n > 2 { n = 1; }
            let p = Pos::new(x as u8, y as u8, name);
            if p.range_to_edge() == 0 && Tile::from_num(n) != Rock {
                set_tile(p, Exit);
            } else {
                set_tile(p, Tile::from_num(n));
            }
        }
    }
}

pub macro info($($t: tt)*) {
    web_sys::console::log_1(&format!("<span style='color:lime'>{}</span>",format!($($t)*)).into())
}
pub macro error($($t: tt)*) {
    web_sys::console::log_1(&format!("<span style='color:red'>{}</span>",format!($($t)*)).into())
}

#[derive(Clone,Copy,PartialEq,Eq,Hash,Default)]
pub struct RoomName(pub u8, pub u8);

impl RoomName {
    pub fn int(self) -> RoomNameInt {
        let half_world_size = 128;
        let x = self.0 as u16 + half_world_size - WORLD_ROOMS as u16;
        let y = self.1 as u16 + half_world_size - WORLD_ROOMS as u16;
        unsafe { transmute(x << 8 | y) }
    }
    
    pub fn adj(self) -> [Self; 8] {
        let mut r = [self; 8];
        let mut i = 0;
        for x in (self.0-1)..=(self.0+1) {
            for y in (self.1-1)..=(self.1+1) {
                if x == self.0 && y == self.1 { continue; }
                r[i] = RoomName(x, y);
                i += 1;
            }
        }
        r
    }
}

impl From<RoomNameInt> for RoomName {
    fn from(int: RoomNameInt) -> Self {
        let half_world_size = 128;
        let packed: u16 = unsafe { transmute(int) };
        let (x, y) = ((packed >> 8) as u8, (packed & 0xFF) as u8);
        RoomName(x - half_world_size + WORLD_ROOMS as u8, y - half_world_size + WORLD_ROOMS as u8)
    }
}
impl fmt::Display for RoomName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}",self.int())
    }
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash,Default)]
pub struct Pos(pub u16, pub u16);

impl Pos {
    pub fn new(x: u8, y: u8, name: RoomName) -> Self {
        Pos(50 * name.0 as u16 + x as u16, 50 * name.1 as u16 + y as u16)
    }
    pub fn room_name(self) -> RoomName {
        RoomName((self.0 / 50) as u8, (self.1 / 50) as u8)
    }
    pub fn position(self) -> Position {
        unsafe {
            Position::new(
                screeps::RoomCoordinate::unchecked_new((self.0 % 50) as u8),
                screeps::RoomCoordinate::unchecked_new((self.1 % 50) as u8),
                self.room_name().int())
        }
    }
    pub fn js_pos(self) -> JsPos {
        self.position().into()
    }
    
    pub fn x(self) -> usize {
        self.0 as usize % 50
    }
    pub fn y(self) -> usize {
        self.1 as usize % 50
    }
    
    pub fn adj(self) -> [Self; 8] {
        [self+TopLeft,self+BottomLeft,self+TopRight,self+BottomRight,
        self+Top,self+Left,self+Bottom,self+Right]
    }
    pub fn orthog_adj(self) -> [Self; 4] {
        [self+Top,self+Left,self+Bottom,self+Right]
    }
    #[allow(unused)]
    pub fn near_to(self, other: Self) -> bool {
        self.range_to(other) <= 1
    }
    pub fn range_to(self, other: Self) -> i16 {
        (self.0 as i16 - other.0 as i16).abs().max((self.1 as i16 - other.1 as i16).abs())
    }
    pub fn orthog_dist(self, other: Self) -> i16 {
        (self.0 as i16 - other.0 as i16).abs() + (self.1 as i16 - other.1 as i16).abs()
    }
    pub fn dir_to(self, other: Self) -> Dir {
        let x = other.0.cmp(&self.0);
        let y = other.1.cmp(&self.1);
        use std::cmp::Ordering::*;
        match (x, y) {
            (Less, Less) => TopLeft,
            (Less, Equal) => Left,
            (Less, Greater) => BottomLeft,
            (Equal, Less) => Top,
            (Equal, Equal) => TopLeft, //STUPID STUPID STUPID STUPID
            (Equal, Greater) => Bottom,
            (Greater, Less) => TopRight,
            (Greater, Equal) => Right,
            (Greater, Greater) => BottomRight,
        }
    }
    pub fn range_to_edge(self) -> usize {
        [self.x(),self.y(),49-self.x(),49-self.y()].iter().cloned().min().unwrap()
    }
    pub fn dir_to_edge(self) -> Dir {
        [(self.x(), Left),(self.y(), Top),(49-self.x(), Right),(49-self.y(), Bottom)].iter().min_by_key(|x| x.0).unwrap().1
    }
    pub fn is_dir_edge(self, dir: Dir) -> bool {
        self.range_to_edge() == 0 && [dir,dir.left(),dir.right()].contains(&self.dir_to_edge())
    }
    
    pub fn relative_to(self, other: Self) -> (i16, i16) {
        (self.0 as i16 - other.0 as i16, self.1 as i16 - other.1 as i16)
    }
}

impl HasPosition for Pos {
    fn pos(&self) -> Position { self.position() }
}

impl From<Position> for Pos {
    fn from(p: Position) -> Self {
        Self::new(p.x().u8(), p.y().u8(), p.room_name().into())
    }
}
impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {}, {})",self.x(),self.y(),self.room_name())
    }
}

impl Add<(i16, i16)> for Pos {
    type Output = Self;
    fn add(self, (x, y): (i16, i16)) -> Self {
        Pos((self.0 as i16 + x) as u16, (self.1 as i16 + y) as u16)
    }
}
impl Sub<(i16, i16)> for Pos {
    type Output = Self;
    fn sub(self, (x, y): (i16, i16)) -> Self {
        Pos((self.0 as i16 - x) as u16, (self.1 as i16 - y) as u16)
    }
}
impl Add<Dir> for Pos {
    type Output = Self;
    fn add(self, dir: Dir) -> Self {
        self + <(i16, i16)>::from(dir)
    }
}
impl Sub<Dir> for Pos {
    type Output = Self;
    fn sub(self, dir: Dir) -> Self {
        self - <(i16, i16)>::from(dir)
    }
}

#[repr(u8)]
#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash)]
pub enum Dir {
    Top = 0,
    TopRight = 1,
    Right = 2,
    BottomRight = 3,
    Bottom = 4,
    BottomLeft = 5,
    Left = 6,
    TopLeft = 7,
}
pub use Dir::*;

impl Dir {
    pub fn left(self) -> Self {
        match self {
            Top => TopLeft,
            TopRight => Top,
            Right => TopRight,
            BottomRight => Right,
            Bottom => BottomRight,
            BottomLeft => Bottom,
            Left => BottomLeft,
            TopLeft => Left,
        }
    }
    pub fn right(self) -> Self {
        match self {
            Top => TopRight,
            TopRight => Right,
            Right => BottomRight,
            BottomRight => Bottom,
            Bottom => BottomLeft,
            BottomLeft => Left,
            Left => TopLeft,
            TopLeft => Top,
        }
    }
    pub fn is_diag(self) -> bool {
        [TopRight,TopLeft,BottomRight,BottomLeft].contains(&self)
    }
}

impl Default for Dir {
    fn default() -> Self {
        Top
    }
}

impl Neg for Dir {
    type Output = Self;
    fn neg(self) -> Self {
        self.right().right().right().right()
    }
}
use screeps::Direction;
impl From<Direction> for Dir {
    fn from(d: Direction) -> Self {
        [Top,TopRight,Right,BottomRight,Bottom,BottomLeft,Left,TopLeft][d as usize - 1]
    }
}
impl From<Dir> for Direction {
    fn from(d: Dir) -> Self {
        use Direction::*;
        [Top,TopRight,Right,BottomRight,Bottom,BottomLeft,Left,TopLeft][d as usize]
    }
}
impl From<Dir> for (i16, i16) {
    fn from(d: Dir) -> Self {
        [(0,-1),(1,-1),(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1)][d as usize]
    }
}

impl From<u16> for Dir {
    fn from(i: u16) -> Self {
        [Top,TopRight,Right,BottomRight,Bottom,BottomLeft,Left,TopLeft][i as usize % 8]
    }
}

impl Add for Dir {
    type Output = (i16, i16);
    fn add(self, other: Self) -> (i16, i16) {
        let (a, b) = self.into();
        let (c, d) = other.into();
        (a + c, b + d)
    }
}

impl Mul<i16> for Dir {
    type Output = (i16, i16);
    fn mul(self, other: i16) -> (i16, i16) {
        let (x, y) = self.into();
        (x * other, y * other)
    }
}

impl fmt::Display for Dir {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Direction::from(*self).fmt(f)
    }
}
